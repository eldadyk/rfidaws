import RPi.GPIO as GPIO 
import SimpleMFRC522 
import requests

my_service = 'localhost:5000/get_owner/'

reader = SimpleMFRC522.SimpleMFRC522() 
try:
    id, text = reader.read()
    print(id)
    print(text)
    res = requests.get(my_service+text)
    print(my_service+text)
    print(res)
finally:
    GPIO.cleanup()
